# คู่มือความปลอดภัยทางดิจิทัลในการชุมนุม ✊ 

##  สิทธิในการชุมนุมกับความปลอดภัยทางดิจิทัล

นับตั้งแต่ช่วงต้นปี 2563 เป็นต้นมา ปรากฏการณ์ของนักเรียน นิสิต นักศึกษาและประชาชนที่ได้ออกมารวมตัวกันชุมนุมเพื่อแสดงออกถึงความไม่พอใจต่อการบริหารประเทศของรัฐบาลได้ขยายกลายเป็นกระแสการชุมนุมขับไล่รัฐบาลที่กระจายไปในหลายพื้นที่ทั่วประเทศไทยอย่างที่ไม่เคยมีมาก่อน กลุ่มคนรุ่นใหม่ที่ส่วนใหญ่เป็นนักเรียนนักศึกษาได้เข้ามามีบทบาทในฐานะแกนนำจัดการชุมนุมมากขึ้น เช่นกรณีนักเรียนหญิงชั้นมัธยมฯปีที่ 2 ลุกขึ้นมาเป็นแกนนำจัดชุมนุมที่พะเยา หนึ่งในข้อสังเกตต่อการชุมนุมของนักเรียน นิสิต นักศึกษาและประชาชนในครั้งนี้ คือการที่โซเชียลมีเดียมีบทบาทอย่างมากในการช่วยขับเคลื่อนขบวนการทางสังคม (mobilize) ให้เกิดการชุมนุมขนาดใหญ่ รวมไปถึงการใช้แฮชแทก (#) ในทวิตเตอร์ได้กลายเป็นช่องทางสำคัญในการกระจายข้อมูลข่าวสาร และเครื่องมือทางดิจิทัลได้ช่วยให้การทำงานจัดชุมนุมง่ายขึ้น

ขณะที่เครื่องมือทางดิจิทัลเป็นหนึ่งปัจจัยสำคัญของขบวนการคนรุ่นใหม่ในการจัดการชุมนุมเพื่อแสดงออกทางการเมือง แต่ยังคงเผชิญหน้ากับอุปสรรคหลายประการ เช่น การถูกดำเนินคดี การถูกปิดกั้นและขัดขวางการจัดกิจกรรม การถูกเจ้าหน้าที่รัฐข่มขู่คุกคาม เช่นเดียวกับการที่นักกิจกรรมต้องเผชิญการคุกคามทางดิจิทัล ดังกรณีที่มีการดำเนินคดีต่อแอดมินเพจที่นัดชุมนุมที่เชียงใหม่ การล่าแม่มดที่มุ่งโจมตีนักกิจกรรม (Online harassment) รวมถึงการทิ้งรอยเท้าดิจิทัล (Digital footprints) นั่นทำให้การติดตามตัวจากพื้นที่ดิจิทัลสู่ความปลอดภัยในโลกออฟไลน์ก็ไม่ใช่เรื่องยาก 

สิ่งที่หลายคนพกติดตัวไปด้วยทุกที่รวมถึงการไปร่วมชุมนุมคือ “โทรศัพท์มือถือ” และข้อมูลสำคัญของคุณและข้อมูลของเพื่อนที่สื่อสารกับคุณก็อยู่ในมือถือด้วย นั่นอาจทำให้มีความเสี่ยงเพิ่มขึ้นถ้าเราไม่ได้มีวิธีการดูแลเรื่องความปลอดภัยของมือถือระหว่างไปร่วมชุมนุม ดังนั้นเพื่อให้การใช้สิทธิในการชุมนุมและเสรีภาพในการแสดงออกของพวกเราเป็นไปอย่างต่อเนื่องและทุกคนมีความปลอดภัยให้มากที่สุด การคำนึงถึงการรักษาความปลอดภัยทางดิจิทัลจึงหนึ่งในปัจจัยสำคัญต่อการเคลื่อนไหวทางสังคมในยุคดิจิทัลเช่นนี้

---------------------------------
##  แนวคิดเรื่องความปลอดภัยทางดิจิทัล คืออะไร

- **การรักษาความปลอดภัยทางดิจิทัล (Digital Security)** มีหลายวิธีการ มีหลายเครื่องมือหรือแอปพลิเคชัน และไม่มีสูตรสำเร็จรูปในการรักษาความปลอดภัยที่ตายตัว ไม่มีแบบแผนที่สมบูรณ์แบบหรือดีที่สุด เพราะวิธีการรักษาความปลอดภัยหรือเครื่องมือที่เราใช้อาจเหมาะสมกับบริบทหนึ่งๆเท่านั้น ซึ่งขึ้นอยู่กับการทำความเข้าใจและการวิเคราะห์ “ภัยคุกคาม” (Threats) ที่เราเผชิญ ขึ้นอยู่กับการประเมิน/ค้นหาและแก้ไข “จุดเปราะบาง” (Vulnerabilities) และเพิ่ม “จุดแข็งหรือศักยภาพ” (Capacities) เพื่อลดความเสี่ยงที่อาจเกิดขึ้น (Risk) ซึ่งการรักษาความปลอดภัยไม่ได้เป็นเรื่องของเครื่องมือเชิงเทคนิคเพียงอย่างเดียว

- _การรักษาความปลอดภัยทางดิจิทัล_ เป็นเรื่องที่ต้องให้ความสำคัญอย่างมีความรับผิดชอบร่วมกัน (Collective responsibility) นั่นหมายถึงการรักษาความปลอดภัยควรเป็นเรื่องของทุกคน เช่น แม้ทุกคนในกลุ่มหรือในองค์กรต่างบอกให้ใช้แอปพลิเคชันที่ปลอดภัยเท่านั้นเมื่อต้องสื่อสารข้อความที่มีความเสี่ยง แต่หากมีหนึ่งคนภายในกลุ่มละเลยเรื่องความปลอดภัย อาจทำให้คนอื่นในกลุ่มตกอยู่ในความเสี่ยงไปด้วย 

- แม้คู่มือฉบับนี้จะเน้นเรื่องความปลอดภัยทางดิจิทัลเป็นหลัก แต่เราจำเป็นต้องคำนึงเรื่องความปลอดภัยเป็นองค์รวม (Holistic security) คือต้องคำนึงเรื่องความปลอดภัยทางดิจิทัลไปพร้อมกับความปลอดภัยด้านกายภาพ (physical) และความปลอดภัยทางสภาวะจิตใจ (psychological) 


- **หัวใจสำคัญของการรักษาความปลอดภัยทางดิจิทัล** คือ “การปกป้องข้อมูลของเรา” (Information security) ซึ่งข้อมูลเหล่านั้น ได้แก่
   1. ข้อมูลการติดต่อ (Contacts) เช่น รายชื่อหรือเบอร์ติดต่อ รวมไปถึงข้อมูลรายละเอียดที่เราติดต่อสื่อสารระหว่างกัน
   1. ตำแหน่งที่อยู่หรือสถานที่ (Location) เช่น ตำแหน่งที่พักอาศัย สถานที่ประชุมเตรียมงาน ตำแหน่งที่อยู่ปัจจุบัน
   1. รหัสผ่าน (Passwords) เช่น รหัสผ่านบัญชีโซเชียลมีเดีย รหัสผ่านบัตรเครดิต รหัสผ่านบัญชีธนาคารออนไลน์
   1. นิสัยหรือพฤติกรรมทางดิจิทัล (Digital habits) เช่น การเช็คอินที่อยู่อาศัยซึ่งง่ายต่อการติดตาม การโพสต์สาธารณะข้อมูลสำคัญที่เกี่ยวกับการทำงานภายในขบวนการเคลื่อนไหว เป็นต้น


- **แล้วข้อมูลเหล่านี้อาจจะอยู่ใน**: 
   1. อุปกรณ์ดิจิทัล (Devices) เช่น โทรศัพท์ แท็บเล็ต คอมพิวเตอร์ เป็นต้น
   1. รูปแบบการสื่อสาร (Communications) เช่น ข้อมูลที่ถูกเก็บบันทึกไว้ในแอปพลิเคชันที่เราใช้ในการสื่อสาร
   1. บัญชีออนไลน์ (Online Accounts) เช่น Gmail, Facebook, Twitter, Instagram, TikTok
   1. ข้อมูลจากการเชื่อมต่ออินเตอร์เน็ต (Internet Traffic) เช่นเครือข่ายผู้ให้บริการอินเตอร์เน็ต (ISP) ที่คุณใช้


- **ดังนั้นการรักษาความปลอดภัยทางดิจิทัล เราควรคำนึงถึง**:
   1. ข้อมูลอะไรบ้างที่เราต้องการแชร์ (What you choose to share) เช่น ภาพถ่าย ตำแหน่งที่อยู่ปัจจุบัน ข้อมูลส่วนตัว (วันเดือนปีเกิด) เป็นต้น
   1. เราเลือกที่จะแชร์ข้อมูลกับใครบ้าง (Who you choose to share with) เช่น การโพสต์ข้อความสำหรับเฉพาะเพื่อน หรือการโพสต์ข้อความสำหรับสาธารณะที่ทุกคนสามารถเข้าถึงข้อความนั้นได้
   1. เราต้องการสื่อสารระหว่างกันอย่างไร (How you communicate) เช่น การสื่อสารผ่านเฟซบุ๊คแบบเปิดสาธารณะหรือเฉพาะคนบางกลุ่ม การเลือกสื่อสารผ่านแอปพลิเคชันที่ปลอดภัย (Signal)
   1. เราจะเลือกกดลิงค์อะไร (What you click) ก่อนคลิกลิ้งค์ใดๆควรทำการตรวจสอบความน่าเชื่อถือและแหล่งที่มาของลิ๊งค์นั้นๆ เพื่อป้องกันการถูกโจมตีจาก phishing link ที่ต้องการเข้าถึงข้อมูลส่วนตัว รหัสผ่านของเรา
   1. เราเลือกใช้บริการอะไร (Which services you choose) คือ การตรวจสอบว่าบริการที่เราใช้มีความปลอดภัยและคำนึงถึงความเป็นส่วนตัวหรือไม่ เช่น แอปพลิเคชันสื่อสารที่เราใช้งานมีความปลอดภัยหรือไม่ หรือถ้าต้องการใช้ VPN บริษัทไหนที่น่าไว้ใจ เป็นต้น

- _สุดท้ายแล้วการรักษาความปลอดภัยทางดิจิทัลในการชุมนุม เราไม่ได้ต้องการให้คุณหยุดทำกิจกรรมเคลื่อนไหว ! แต่เราอยากชวนทุกคนทำความเข้าใจ/ประเมินความเสี่ยงอย่างรอบด้าน และร่วมกันค้นหาวิธีการรับมือภัยคุกคามที่อาจเกิดขึ้นได้อย่างเหมาะสมกับตนเอง เพื่อทำให้การชุมนุมของพวกเรามีความปลอดภัยด้านดิจิทัลมากขึ้น_   
   **Be safe and resilient!**
---------------------------------
# Resources: Digital rights and digital safety_Thailand

**IN ENGLISH**

**Guideline, manuals, toolkits**

- [Cyber Safety Handbook for Students of Secondary & Senior Secondary Schools-2020](http://cbseacademic.nic.in/web_material/Manuals/Cyber_Safety_Manual.pdf)
- [Security Training for Everyone](https://sudo.pagerduty.com/for_everyone/)
- [‘Hate Speech’ Explained: A Toolkit](https://www.article19.org/resources/hate-speech-explained-a-toolkit)
- [Holistic Security Trainer Manual](file:///C:/Users/Git%20Jirasamatakij/OneDrive/Documents/EM-Git/Reading%20Material/Session1/holisticsecurity_trainersmanual.pdf)
- [Digital Rights in SouthEastAsia](file:///C:/Users/Git%20Jirasamatakij/OneDrive/Documents/EM-Git/Reading%20Material/Digital-Rights-in-Southeast-Asia-Conceptual-Framework-and-Movement-Building.pdf)
- [Gender Approaches to Cybersecurity](file:///C:/Users/Git%20Jirasamatakij/OneDrive/Documents/EM-Git/Reading%20Material/Gender%20Approaches%20to%20Cybersecurity_Digital_Final.pdf)
- [Safe Basic Training Curriculum](file:///C:/Users/Git%20Jirasamatakij/OneDrive/Documents/EM-Git/Reading%20Material/safe-basic-training-curriculum-plus-annex.pdf)
- [Approaches to Adult Learning](https://www.level-up.cc/before-an-event/levelups-approach-to-adult-learning/)
- [Gender and Technology Training Curriculum](https://en.gendersec.train.tacticaltech.org/)
- [FTX Platform](https://en.ftx.apc.org/)
- [CPJ Journalist Security Guide](https://cpj.org/reports/2012/04/journalist-security-guide/)
- [Digital Safety: Protecting against targeted online attacks](https://cpj.org/2020/05/digital-safety-protecting-against-targeted-online-attacks/)
- [Your Security Plan by EFF](https://ssd.eff.org/en/module/your-security-plan)
- [FTX Safety Reboot](https://en.ftx.apc.org/)
- [Gender and Technology Institute Training Curriculum](https://en.gendersec.train.tacticaltech.org/)
- [Security Training for Everyone](https://sudo.pagerduty.com/for_everyone/)

**News and articles**

- [UPR: Can the Universal Periodic Review hold governments accountable on digital rights?](https://www.openglobalrights.org/can-the-universal-periodic-review-hold-governments-accountable-on-digital-rights/#:~:text=The%20Universal%20Periodic%20Review%20can,should%20be%20protected%20and%20promoted.&text=The%20UPR%20provides%20the%20opportunity,rights%20situations%20in%20their%20countries)
- [Petition against sexual content deep fake in South Korea](https://www1.president.go.kr/petitions/595595)
- [The Inside Story of How Signal Became the Private Messaging App for an Age of Fear and Distrust-Sep 2020](https://time.com/5893114/signal-app-privacy/)
- [Digital resistance: security & privacy tips from Hong Kong protesters - Oct 2019](https://medium.com/crypto-punks/digital-resistance-security-privacy-tips-from-hong-kong-protesters-37ff9ef73129)
- [Here’s What the Big Tech Companies Know About You- Nov 2018](https://www.visualcapitalist.com/heres-what-the-big-tech-companies-know-about-you/?fbclid=IwAR063HX8qC57gSQF1gygJfS0aKNbHdkU9mIcjBE-eyUOgGeWrsyV2P67ZpQ)
- [The Global Expansion of AI Surveillance](https://carnegieendowment.org/2019/09/17/global-expansion-of-ai-surveillance-pub-79847)
- [Gender Approaches to Cybersecurity](https://unidir.org/publication/gender-approaches-cybersecurity)
- [Technology is Stupid](https://lite.tacticaltech.org/news/technology-is-stupid/)
- [Safety First](https://kit.exposingtheinvisible.org/en/safety.html)
- [How to Master Secret Work](https://cryptome.org/anc-manual.htm)
- [Feminist Principles of the Internet](https://feministinternet.org/sites/default/files/Feminist_principles_of_the_internetv2-0.pdf)
- [Gender Approaches in Cybersecurity](https://unidir.org/publication/gender-approaches-cybersecurity)
- [Why MetaData Matters](https://ssd.eff.org/en/module/why-metadata-matters)
- [Remaining Anonymous and Bypass censorship on the internet](https://securityinabox.org/en/guide/anonymity-and-circumvention/)
- [Security Scenario](https://ssd.eff.org/en/module-categories/security-scenarios)
- [Metanull](https://securityinabox.org/en/lgbti-mena/metanull/windows/)
- [How to be completely Anonymous on the internet in 2021](https://medium.com/dsc-rngpit/how-to-be-completely-anonymous-on-the-internet-in-2020-60af1f30715e)


**Books & e-books**

- [Twitter and Tear Gas: The Power and Fragility of Networked Protest” by Zeynep Tufekci about protest in the age of the internet](https://www.twitterandteargas.org/downloads/twitter-and-tear-gas-by-zeynep-tufekci.pdf)

**Resources pages**

- [The Web This Week, your weekly briefing on the digital tech changing our world](https://us8.campaign-archive.com/?u=b3c8e6e91fe1905e99f8b59fd&id=5921da686f)
- [Discover evidence of Internet Censorship Worldwide](https://explorer.ooni.org/)

**Thai issues**

- [Youth Activists’ struggle with mental health in the ongoing protests in Thailand (6 Jan 2021)](https://prachatai.com/english/node/9001)
- [Thailand's protests and their digital dimension](https://www.dw.com/en/thailands-protests-and-their-digital-dimension/a-55315079)
- [#WhatsHappeningInThailand: the power dynamics of Thailand’s digital activism-Dec 2020](https://www.aspi.org.au/report/whatshappeninginthailand-power-dynamics-thailands-digital-activism)

**Videos**

- [Thailand’s youth rebellion and the monarchy - BBC News](https://www.youtube.com/watch?v=v7VF55cyDUc&feature=youtu.be)
- [How Does the Internet Work?](https://www.youtube.com/watch?v=TNQsmPf24go&ab_channel=Vox)
- [How The Internet Work VDO by Totem](https://learn.totem-project.org/courses/course-v1:Totem+TP)

**Recommended Movies & Documentaries**

- Black Code - 2016 (documentary 75 minutes): Surveillance. A look at the global impact the internet has on free speech, privacy and activism. How governments are manipulating the Internet to control their citizens.: เกี่ยวกับการสอดแนมที่ทำโดยรัฐบาล รวมไปถึงการถกเถียงเรื่องเสรีภาพในการพูดและความเป็นส่วนตัว 
- Zero Days - 2016 (documentary): Cyber way and the malware worm Stuxnet: สหรัฐใช้ไวรัสโจมตีโครงสร้างพื้นฐานของรัฐบาลฝ่ายตรงข้าม
- We Are Legion: The Story of the Hacktivists - 2012
- Lo & Behold: Reveries of the Connected World - 2016
- The Fifth Estate - 2013: เกี่ยวกับ WikiLeaks founder Julian Assange
- We Steal Secrets: The Story of WikiLeaks-2013: Julian Assange
- The Circle - 2017: หนังที่แสดงโดย Emma Watson & Tom Hanks เล่าถึงการพัฒนาเทคโนโลยีแล้วมีผลต่อชีวิตคนเราได้อย่างไร
- Terms and Conditions May Apply-2013: 
- Citizenfour - 2014: Edward Snowden 
- The Great Hack - 2019: การเปิดโปง Cambridge Analytica 
- The Social Dilemma - 2020
- Coded Bias (Documentary) - 2020

------------------------------------------------------------------
**IN THAI**

**News and articles**

- [ปัญญาประดิษฐ์กับอคติทางสังคม-วัฒนธรรม” 
โดย ศาสตราจารย์ ดร.นิติ ภวัครพันธุ์ อาจารย์พิเศษ ภาควิชาสังคมวิทยาและมานุษยวิทยา คณะรัฐศาสตร์ จุฬาลงกรณ์มหาวิทยาลัย](https://so06.tci-thaijo.org/index.php/jasac/article/view/246488/166983?fbclid=IwAR2h9FpJ6u-D-5P57-v8NU7wzBdeaMj3qxgP1XIp8s9tAEOjrEFoXyRxSTQ)
- [ชาวทวิตเตอร์เริ่มเขย่าการเมืองได้อย่างไร? พลังติ่งเกาหลี ‘น่ากลัว’ แค่ไหน?-2021-01-13](https://prachatai.com/journal/2021/01/91178)
- [วางข้อมูลส่วนบุคคลบนตาชั่ง แล้วปฏิบัติต่อกันอย่างรับผิดชอบ](https://waymagazine.org/thitirat-thipsumritkul-interview/)
- [มนุษย์ในโลกดิจิทัล DIGITAL ANTHROPOLOGY](https://researchcafe.org/digital-anthropology/)
- [Digital Transformation แบบไทยๆ : สิทธิเสรีภาพ ความปลอดภัย และความมั่นคง](https://www.the101.world/freedom-and-cyber-security-in-digital-transformation/)

**Guideline, manuals, toolkits**

- [คู่มือพลเมืองเน็ต: เข้าใจเน็ต และใช้เน็ตให้ปลอดภัย](https://thainetizen.org/docs/netizen-handbook-safe-internet/)
- [ใช้เน็ตปลอดภัย อุ่นใจด้วย HTTPS Everywhere](https://thainetizen.org/https-everywhere/)
- [การ์ดช่วยจำ “ปลอดภัยทันใจ” Security-in-a-Box flash cards](https://thainetizen.org/docs/security-in-a-box-flash-cards/)
- [คำแนะนำเบื้องต้นกรณีถูกเจ้าหน้าที่รัฐควบคุมตัวจากการโพสต์หรือแชร์ข้อความในโซเชียลมีเดีย](https://tlhr2014.files.wordpress.com/2015/12/2015-12-17_guideline-on-arrested-from-using-social-media.pdf)
- [Cyber threats ภัยคุกคามไซเบอร์ในไทย](https://www.thaicert.or.th/downloads/files/Cyber_Threats_2014.pdf)
- [แหล่งรวมข้อมูลของ ETDA](https://www.etda.or.th/documents-for-download.html)
- [HTTPS ปลอดภัยยังไง](https://medium.com/odds-team/https-%E0%B8%9B%E0%B8%A5%E0%B8%AD%E0%B8%94%E0%B8%A0%E0%B8%B1%E0%B8%A2%E0%B8%A2%E0%B8%B1%E0%B8%87%E0%B9%84%E0%B8%87-5caa5520c15d)
- [th-digital-survival](https://github.com/mishari/th-digital-survival)


**Videos**

- [FIDH: Women pro-democracy activists criminalized, harassed-Feb 2021](https://www.youtube.com/watch?v=DebieLfw5ig&feature=youtu.be)
- [เหตุผลที่เราต้องช่วยกันส่งเสียง อย่าให้ sexual harassment เป็นเรื่องปกติ](https://www.facebook.com/watch/?v=1342512379438213)
- [อะไรคือความเป็นส่วนตัว | Privacy International](https://www.youtube.com/watch?v=rJPndjLogaU)


------------------------------------------------------------------


